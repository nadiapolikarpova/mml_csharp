﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics.Contracts;

namespace MML
{
	/// <summary>
	/// Finite relation
	/// </summary>
	/// <typeparam name="L">Domain type of the relation</typeparam>
	/// <typeparam name="R">Range type of the relation</typeparam>
	public class Relation<L, R>
	{

//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------

		/// <summary>
		/// Create an empty relation
		/// </summary>
		[Pure]
		public Relation()
		{
			lefts = new L[0];
			rights = new R[0];
		}

		/// <summary>
		/// Create a singleton relation
		/// </summary>
		/// <param name="left">Left component of the only element</param>
		/// <param name="right">Right component of the only element</param>
		[Pure]
		public Relation(L left, R right)
		{
			lefts = new L[1];
			lefts[0] = left;
			rights = new R[1];
			rights[0] = right;
		}

		// make from array
		/// <summary>
		/// Create relation with predefined storage
		/// </summary>
		/// <param name="left">Left components</param>
		/// <param name="right">Right components</param>
		[Pure]
		internal Relation(L[] lefts, R[] rights)
		{
			// ks_exists, vs_exists
			Contract.Requires(lefts != null);
			Contract.Requires(rights != null);
			this.lefts = lefts;
			this.rights = rights;
		}


//------------------------------------------------------------------------------
// Properties
//------------------------------------------------------------------------------

		/// <summary>
		/// Is <paramref name="x"/> related to <paramref name="y"/>?
		/// </summary>
		[Pure]
		public bool Has(L x, R y)
		{
			return ImageOf(x).Has(y);
		}

		/// <summary>
		/// Indexer alias for Has
		/// </summary>
		[Pure]
		public bool this[L x, R y]
		{
			get { return Has(x, y); }
		}

		/// <summary>
		/// Is map empty?
		/// </summary>
		/// <returns></returns>
		[Pure]
		public bool IsEmpty()
		{
			return lefts.Length == 0;
		}


//------------------------------------------------------------------------------
// Sets
//------------------------------------------------------------------------------

		/// <summary>
		/// The set of left components 
		/// </summary>
		[Pure]
		public Set<L> Domain
		{
			get { return new Set<L>(lefts.Distinct().ToArray()); }
		}

		/// <summary>
		/// The set of right components
		/// </summary>
		[Pure]
		public Set<R> Range
		{
			get { return new Set<R>(rights.Distinct().ToArray()); }
		}

		/// <summary>
		/// Set of values related to <paramref name="x"/>
		/// </summary>
		[Pure]
		public Set<R> ImageOf(L x)
		{
			return Image(new Set<L>(x));
		}

		/// <summary>
		/// Set of values related to any value in <paramref name="subdomain"/>
		/// </summary>
		[Pure]
		public Set<R> Image(Set<L> subdomain)
		{
			Contract.Requires(!ReferenceEquals(subdomain, null));
			return (Restricted(subdomain)).Range;

		}


//------------------------------------------------------------------------------
// Measurement
//------------------------------------------------------------------------------

		/// <summary>
		/// Cardinality
		/// </summary>
		public int Count
		{
			get { return lefts.Length; }
		}


//------------------------------------------------------------------------------
// Comparison
//------------------------------------------------------------------------------

		/// <summary>
		/// Is <paramref name="obj"/> a relation with the same pairs?
		/// </summary>
		[Pure]
		public override bool Equals(object obj)
		{
			Relation<L, R> rel = obj as Relation<L, R>;
			return !ReferenceEquals(rel, null) && Count == rel.Count &&
				Enumerable.Range(0, lefts.Length).All(i => rel[lefts[i], rights[i]]); 
		}

		// Not part of API => no xml comment
		public override int GetHashCode()
		{
			return lefts.GetHashCode() ^ rights.GetHashCode();
		}

		/// <summary>
		/// Are the two objects equal? (Operator is equivalent to the |=| alias in Eiffel)
		/// </summary>
		[Pure]
		public static bool operator ==(Relation<L, R> r1, Relation<L, R> r2)
		{
			return (ReferenceEquals(r1, null) & ReferenceEquals(r2, null)) |
				(!ReferenceEquals(r1, null) && r1.Equals(r2));
		}

		/// <summary>
		/// Are the two objects not equal?
		/// </summary>
		[Pure]
		public static bool operator !=(Relation<L, R> r1, Relation<L, R> r2)
		{
			return !(r1 == r2);
		}


//------------------------------------------------------------------------------
// Modification
//------------------------------------------------------------------------------

		/// <summary>
		/// Current relation extended with pair (<paramref name="x"/>, <paramref name="y"/>) if absent
		/// </summary>
		public Relation<L, R> Extended(L x, R y)
		{
			if (!this.Has(x, y))
			{
				L[] ls = new L[lefts.Length + 1];
				Array.Copy(lefts, ls, lefts.Length);
				ls[ls.Length - 1] = x;

				R[] rs = new R[rights.Length + 1];
				Array.Copy(rights, rs, rights.Length);
				rs[rs.Length - 1] = y;

				return new Relation<L, R>(ls, rs);
			}
			else
			{
				return this;
			}
		}

		/// <summary>
		/// Current relation with pair (<paramref name="x"/>, <paramref name="y"/>) removed if present
		/// </summary>
		public Relation<L, R> Removed(L x, R y)
		{
			int i = Array.IndexOf(lefts, x);
			if (i >= 0 && Equals(rights[i], y))
			{
				L[] ls = new L[lefts.Length - 1];
				Array.Copy(lefts, 0, ls, 0, i);
				Array.Copy(lefts, i + 1, ls, i, lefts.Length - i - 1);

				R[] rs = new R[rights.Length - 1];
				Array.Copy(rights, 0, rs, 0, i);
				Array.Copy(rights, i + 1, rs, i, rights.Length - i - 1);

				return new Relation<L, R>(ls, rs);
			}
			else
			{
				return this;
			}
		}

		/// <summary>
		/// Relation that consists of all pairs in this relation whose left component is in <paramref name="subdomain"/>.
		/// </summary>
		public Relation<L, R> Restricted(Set<L> subdomain)
		{
			Contract.Requires(!ReferenceEquals(subdomain, null));

			L[] ls = new L[lefts.Length];
			R[] rs = new R[rights.Length];
			int j = 0;

			for (int i = 0; i < lefts.Length; i++)
			{
				if (subdomain[lefts[i]])
				{
					ls[j] = lefts[i];
					rs[j] = rights[i];
					j++;
				}
			}
			Array.Resize(ref ls, j);
			Array.Resize(ref rs, j);
			return new Relation<L, R>(ls, rs);
		}

		/// <summary>
		/// Operator equivalent for restricted operation <seealso cref="Restricted(Set<L>)"></seealso>>
		/// </summary>
		public static Relation<L, R> operator |(Relation<L, R> r, Set<L> s)
		{
			Contract.Requires(!ReferenceEquals(r, null));
			Contract.Requires(!ReferenceEquals(s, null));
			return r.Restricted(s);
		}

		/// <summary>
		/// Relation that consists of inverted pairs from this relation
		/// </summary>
		public Relation<R, L> Inverse()
		{
			return new Relation<R, L>(rights, lefts);
		}

		/// <summary>
		/// Relation that consists of pairs contained in either this relation or <paramref name="other"/>
		/// </summary>
		public Relation<L, R> Union(Relation<L, R> other)
		{
			Contract.Requires(!ReferenceEquals(other, null));

			// ref = this - other
			Relation<L, R> res = Difference(other);

			// ref = ref + other
			Array.Resize(ref res.lefts, res.lefts.Length + other.lefts.Length);
			Array.Resize(ref res.rights, res.rights.Length + other.rights.Length);
			Array.Copy(other.lefts, 0, res.lefts, res.lefts.Length - other.lefts.Length, other.lefts.Length);
			Array.Copy(other.rights, 0, res.rights, res.rights.Length - other.rights.Length, other.rights.Length);
			return res;
		}

		/// <summary>
		/// Operator equivalent for union operation <seealso cref="Union(Relation<L, R>)"></seealso>>
		/// </summary>
		public static Relation<L, R> operator +(Relation<L, R> r1, Relation<L, R> r2)
		{
			Contract.Requires(!ReferenceEquals(r1, null));
			Contract.Requires(!ReferenceEquals(r2, null));
			return r1.Union(r2);
		}

		/// <summary>
		/// Relation that consists of pairs contained in both this relation and <paramref name="other"/>
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		public Relation<L, R> Intersection(Relation<L, R> other)
		{
			Contract.Requires(!ReferenceEquals(other, null));
            
			L[] ls = new L[Count];
			R[] rs = new R[Count];
			int j = 0;

			for (int i = 0; i < Count; i++)
			{
				if (other[lefts[i], rights[i]])
				{
					ls[j] = lefts[i];
					rs[j] = rights[i];
					j++;
				}
			}

			Array.Resize(ref ls, j);
			Array.Resize(ref rs, j);
			return new Relation<L, R>(ls, rs);
		}

		/// <summary>
		/// Operator equivalent for intersection operation <seealso cref="Intersection(Relation<L, R>)"></seealso>>
		/// </summary>
		public static Relation<L, R> operator *(Relation<L, R> r1, Relation<L, R> r2)
		{
			Contract.Requires(!ReferenceEquals(r1, null));
			Contract.Requires(!ReferenceEquals(r2, null));
			return r1.Intersection(r2);
		}

		/// <summary>
		/// Set of values contained in this relation but not in <paramref name="other"/>
		/// </summary>
		public Relation<L, R> Difference(Relation<L, R> other)
		{
			Contract.Requires(!ReferenceEquals(other, null));

			L[] ls = new L[Count];
			R[] rs = new R[Count];
			int j = 0;

			for (int i = 0; i < Count; i++)
			{
				if (!other[lefts[i], rights[i]])
				{
					ls[j] = lefts[i];
					rs[j] = rights[i];
					j++;
				}
			}

			Array.Resize(ref ls, j);
			Array.Resize(ref rs, j);
			return new Relation<L, R>(ls, rs);
		}

		/// <summary>
		/// Operator equivalent for difference operation <seealso cref="Difference(Relation<L, R>)"></seealso>>
		/// </summary>
		public static Relation<L, R> operator -(Relation<L, R> r1, Relation<L, R> r2)
		{
			Contract.Requires(!ReferenceEquals(r1, null));
			Contract.Requires(!ReferenceEquals(r2, null));
			return r1.Difference(r2);
		}

		/// <summary>
		/// Relation that consists of pairs contained in either this relation or <paramref name="other"/>, but not in both
		/// </summary>
		public Relation<L, R> SymDifference(Relation<L, R> other)
		{
			Contract.Requires(other != null);
			return Union(other).Difference(Intersection(other));
		}

		/// <summary>
		/// Operator equivalent for symmetric difference operation <seealso cref="SymDifference(Relation<L, R>)"></seealso>>
		/// </summary>
		public static Relation<L, R> operator ^(Relation<L, R> r1, Relation<L, R> r2)
		{
			Contract.Requires(!ReferenceEquals(r1, null));
			Contract.Requires(!ReferenceEquals(r2, null));
			return r1.SymDifference(r2);
		}


//------------------------------------------------------------------------------
// Implementation
//------------------------------------------------------------------------------

		// Storage for left components of pairs
		internal L[] lefts;

		// Storage for right components of pairs
		internal R[] rights;


//------------------------------------------------------------------------------
// Invariant
//------------------------------------------------------------------------------

		[ContractInvariantMethod]
		private void ObjectInvariant()
		{
			Contract.Invariant(lefts != null);
			Contract.Invariant(rights != null);
			Contract.Invariant(lefts.Length == rights.Length);
		}
	}
}
