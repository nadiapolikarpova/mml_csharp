﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics.Contracts;

namespace MML
{
	/// <summary>
	/// Finite bag
	/// </summary>
	/// <typeparam name="T">Type of bag elements</typeparam>
	public class Bag<T>
	{
//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------

		// default_create

		/// <summary>
		/// Create an empty bag
		/// </summary>
		[Pure]
		public Bag()
		{
			keys = new T[0];
			values = new int[0];
			count = 0;
		}

		// singleton

		/// <summary>
		/// Create a singleton bag
		/// </summary>
		/// <param name="x">The only element in the bag</param>
		[Pure]
		public Bag(T x)
			: this(x, 1)
		{ }

		// multiple

		/// <summary>
		/// Create a bag with multiple occurrences of the same element
		/// </summary>
		/// <param name="x">The element of the bag</param>
		/// <param name="n">Number of occurrences</param>
		[Pure]
		public Bag(T x, int n)
		{
			// n_positive: n >= 0
			Contract.Requires(n >= 0);

			keys = new T[1];
			keys[0] = x;
			values = new int[1];
			values[0] = n;
			count = n;
		}

		// make_from_arrays (internal/mml_model)

		/// <summary>
		/// Create a bag with predefined key and value storage
		/// </summary>
		/// <param name="keys">Key storage</param>
		/// <param name="values">Values storage</param>
		/// <param name="count">Total number of elements</param>
		[Pure]
		internal Bag(T[] keys, int[] values, int count)
		{
			// ks_exists, vs_exists
			Contract.Requires(keys != null);
			Contract.Requires(values != null);
			Contract.Requires(keys.Length == values.Length);
			// ks_has_no_duplicates
			Contract.Requires(keys.Distinct().Count() == keys.Length);
			// vs_positive
			Contract.Requires(Array.TrueForAll(values, y => y > 0));			
			// not in eiffel (but probably useful)
			Contract.Requires(values.Sum() == count);

			this.keys = keys;
			this.values = values;
			this.count = count;
		}


//------------------------------------------------------------------------------
// Properties
//------------------------------------------------------------------------------

		/// <summary>
		/// Is <paramref name="x"/> contained?
		/// </summary>
		[Pure]
		public bool Has(T x)
		{
			return keys.Contains(x);
		}

		/// <summary>
		///  Is bag empty?
		/// </summary>
		/// <returns></returns>
		[Pure]
		public bool IsEmpty()
		{
			return keys.Length == 0;
		}

		/// <summary>
		/// Are all values equal to <paramref name="c"/>?
		/// </summary>
		[Pure]
		public bool IsConstant(int c)
		{
			return Array.TrueForAll(values, x => Equals(x, c));
		}


//------------------------------------------------------------------------------
// Sets
//------------------------------------------------------------------------------

		/// <summary>
		/// Set of values that occur at least once
		/// </summary>
		[Pure]
		public Set<T> Domain
		{
			get { return new Set<T>(keys); }
		}


//------------------------------------------------------------------------------
// Measurement
//------------------------------------------------------------------------------

		/// <summary>
		/// How many times <paramref name="x"/> appears
		/// </summary>
		[Pure]
		public int Occurrences(T x)
		{
			int i = Array.IndexOf(keys, x);
			return (i >= 0) ? values[i] : 0;
		}

		/// <summary>
		/// Indexer alias for Occurrences
		/// </summary>
		[Pure]
		public int this[T x]
		{
			get { return Occurrences(x); }
		}

		/// <summary>
		/// Total number of elements
		/// </summary>
		[Pure]
		public int Count
		{
			// Useful?
			get { return count; }
		}


//------------------------------------------------------------------------------
// Comparison
//------------------------------------------------------------------------------

		// is_model_equal alias "|=|" (other: MML_MODEL): BOOLEAN
		/// <summary>
		/// Is <paramref name="obj"/> a bag with the same elements?
		/// </summary>
		[Pure]
		public override bool Equals(object obj)
		{
			// A faster cast operator and lambda expressions
			Bag<T> bag = obj as Bag<T>;
			return !ReferenceEquals(bag, null) && Count == bag.Count &&
				Enumerable.Range(0, keys.Length).All(i => bag[keys[i]] == values[i]);
		}

		// Not part of API => no xml comment
		[Pure]
		public override int GetHashCode()
		{
			return keys.GetHashCode() ^ values.GetHashCode();
		}

		/// <summary>
		/// Are the two objects equal? (Operator is equivalent to the |=| alias in Eiffel)
		/// </summary>
		[Pure]
		public static bool operator ==(Bag<T> b1, Bag<T> b2)
		{
			return (ReferenceEquals(b1, null) & ReferenceEquals(b2, null)) |
				(!ReferenceEquals(b1, null) && b1.Equals(b2));
		}

		/// <summary>
		/// Are the two objects not equal?
		/// </summary>
		[Pure]
		public static bool operator !=(Bag<T> b1, Bag<T> b2)
		{
			return !(b1 == b2);
		}


//------------------------------------------------------------------------------
// Modification
//------------------------------------------------------------------------------

		/// <summary>
		/// Current bag extended with one occurrence of <paramref name="x"/>
		/// </summary>
		[Pure]
		public Bag<T> Extended(T x)
		{
			return ExtendedMultiple(x, 1);
		}

		/// <summary>
		/// Operator equivalent for extended operation <seealso cref="Extended(T)"></seealso>>
		/// </summary>
		[Pure]
		public static Bag<T> operator &(Bag<T> b1, T t)
		{
			Contract.Requires(!ReferenceEquals(b1, null));
			Contract.Requires(!ReferenceEquals(t, null));
			return b1.Extended(t);
		}

		/// <summary>
		/// Current bag extended with <paramref name="n"/> occurrences of <paramref name="x"/>
		/// </summary>
		[Pure]
		public Bag<T> ExtendedMultiple(T x, int n)
		{
			Contract.Requires(n >= 0);

			if (n > 0)
			{
				int i = Array.IndexOf(keys, x);
				if (i >= 0)
				{
					// Key already in bag
					int[] vs = new int[values.Length];
					Array.Copy(values, vs, values.Length);
					vs[i] += n;

					return new Bag<T>(keys, vs, count + n);
				}
				else
				{
					// New key, need to modify keys as well
					T[] ks = new T[keys.Length + 1];
					Array.Copy(keys, ks, keys.Length);
					ks[ks.Length - 1] = x;

					int[] vs = new int[values.Length + 1];
					Array.Copy(values, vs, values.Length);
					vs[vs.Length - 1] = n;

					return new Bag<T>(ks, vs, count + n);
				}
			}
			else
			{
				return this;
			}
		}

		// no '/' alias
		/// <summary>
		/// Current bag with one occurrence of <paramref name="x"/> removed if contained
		/// </summary>
		[Pure]
		public Bag<T> Removed(T x)
		{
			return RemovedMultiple(x, 1);
		}

		/// <summary>
		/// Operator equivalent for removed operation <seealso cref="Removed(T)"></seealso>>
		/// </summary>
		[Pure]
		public static Bag<T> operator /(Bag<T> b1, T t)
		{
			Contract.Requires(!ReferenceEquals(b1, null));
			Contract.Requires(!ReferenceEquals(t, null));
			return b1.Removed(t);
		}

		/// <summary>
		/// Current bag with at most <paramref name="n"/> occurrence of <paramref name="x"/> removed if contained
		/// </summary>
		[Pure]
		public Bag<T> RemovedMultiple(T x, int n)
		{
			Contract.Requires(n >= 0);

			int i = Array.IndexOf(keys, x);
			if (n == 0 || i < 0)
			{
				return this;
			}
			else if (values[i] <= n)
			{
				// Remove key completely
				T[] ks = new T[keys.Length - 1];
				Array.Copy(keys, 0, ks, 0, i);
				Array.Copy(keys, i + 1, ks, i, keys.Length - i - 1);

				int[] vs = new int[values.Length - 1];
				Array.Copy(values, 0, vs, 0, i);
				Array.Copy(values, i + 1, vs, i, values.Length - i - 1);

				return new Bag<T>(ks, vs, count - n);
			}
			else
			{
				// Just decrease occurrence count
				int[] vs = new int[values.Length];
				Array.Copy(values, vs, values.Length);
				vs[i] -= n;
				return new Bag<T>(keys, vs, count - n);
			}
		}

		/// <summary>
		/// Current bag with all occurrences of <paramref name="x"/> removed, if contained
		/// </summary>
		[Pure]
		public Bag<T> RemovedAll(T x)
		{
			return RemovedMultiple(x, this[x]);
		}

		/// <summary>
		/// Bag that consists of all elements of this bag that are in <paramref name="subdomain"/>.
		/// </summary>
		[Pure]
		public Bag<T> Restricted(Set<T> subdomain)
		{
			Contract.Requires(!ReferenceEquals(subdomain, null));

			T[] ks = new T[keys.Length];
			int[] vs = new int[values.Length];
			int j = 0;
			int c = 0;

			for (int i = 0; i < keys.Length; i++)
			{
				if (subdomain[keys[i]])
				{
					ks[j] = keys[i];
					vs[j] = values[i];
					c = c + values[i];
					j++;
				}
			}
			Array.Resize(ref ks, j);
			Array.Resize(ref vs, j);
			return new Bag<T>(ks, vs, c);
		}

		/// <summary>
		/// Operator equivalent for restricted operation <seealso cref="Restricted(Set<T>)"></seealso>>
		/// </summary>
		[Pure]
		public static Bag<T> operator |(Bag<T> b, Set<T> s)
		{
			Contract.Requires(!ReferenceEquals(b, null));
			Contract.Requires(!ReferenceEquals(s, null));
			return b.Restricted(s);
		}

		/// <summary>
		/// Bag that contains all elements from this bag and <paramref name="other"/>
		/// </summary>
		[Pure]
		public Bag<T> Union(Bag<T> other)
		{
			Contract.Requires(!ReferenceEquals(other, null));

			T[] ks = new T[keys.Length + other.keys.Length];
			int[] vs = new int[values.Length + other.values.Length];
			Array.Copy(keys, ks, keys.Length);
			Array.Copy(values, vs, values.Length);

			int j = keys.Length;
			int k;

			for (int i = 0; i < other.keys.Length; i++)
			{
				k = Array.IndexOf(keys, other.keys[i]);
				if (k >= 0)
				{
					vs[k] += other.values[i];
				}
				else
				{
					ks[j] = other.keys[i];
					vs[j] = other.values[i];
					j++;
				}
			}

			Array.Resize(ref ks, j);
			Array.Resize(ref vs, j);
			return new Bag<T>(ks, vs, count + other.count);
		}

		[Pure]
		public static Bag<T> operator +(Bag<T> b1, Bag<T> b2)
		{
			Contract.Requires(!ReferenceEquals(b1, null));
			Contract.Requires(!ReferenceEquals(b2, null));
			return b1.Union(b2);
		}

		/// <summary>
		/// This bag with all occurrences of values from <paramref name="other"/> removed
		/// </summary>
		[Pure]
		public Bag<T> Difference(Bag<T> other)
		{
			Contract.Requires(!ReferenceEquals(other, null));

			T[] ks = new T[keys.Length];
			int[] vs = new int[values.Length];
			int k, j = 0, c = 0;

			for (int i = 0; i < keys.Length; i++)
			{
				k = other[keys[i]];
				if (k < values[i])
				{
					ks[j] = keys[i];
					vs[j] = values[i] - k;
					c = c + vs[j];
					j++;
				}
			}

			Array.Resize(ref ks, j);
			Array.Resize(ref vs, j);
			return new Bag<T>(ks, vs, c);
		}

		[Pure]
		public static Bag<T> operator -(Bag<T> b1, Bag<T> b2)
		{
			Contract.Requires(!ReferenceEquals(b1, null));
			Contract.Requires(!ReferenceEquals(b2, null));
			return b1.Difference(b2);
		}


//------------------------------------------------------------------------------
// Implementation
//------------------------------------------------------------------------------

		// Key storage
		internal T[] keys;

		// Occurrences storage
		internal int[] values;

		// Total number of elements
		internal int count;


//------------------------------------------------------------------------------
// Invariant
//------------------------------------------------------------------------------
		[ContractInvariantMethod]
		private void ObjectInvariant()
		{
			Contract.Invariant(keys != null);
			Contract.Invariant(values != null);
			Contract.Invariant(keys.Length == values.Length);
			// ks_has_no_duplicates
			Contract.Invariant(keys.Distinct().Count() == keys.Length);
			// vs_positive
			Contract.Invariant(Array.TrueForAll(values, y => y > 0));
			// not in eiffel (but probably useful)
			Contract.Invariant(values.Sum() == count);
		}
	}
}
