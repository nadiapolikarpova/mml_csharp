﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics.Contracts;


namespace MML
{
	/// <summary>
	/// Finite sequence.
	/// Indexing starts from 0.
	/// </summary>
	/// <typeparam name="T">Type of sequence elements</typeparam>
	public class Sequence<T> : System.Collections.IEnumerable
	{
		public System.Collections.IEnumerator GetEnumerator()
		{
			return array.GetEnumerator();
		}

//------------------------------------------------------------------------------
// Initializiation
//------------------------------------------------------------------------------

		/// <summary>
		/// Create an empty sequence
		/// </summary>
		[Pure]
		public Sequence()
		{
			array = new T[0];
		}

		/// <summary>
		/// Create a singleton sequence
		/// </summary>
		/// <param name="x">The only element in the sequence</param>
		[Pure]
		public Sequence(T x)
		{
			array = new T[1];
			array[0] = x;
		}

		// make_from_array
		/// <summary>
		/// Create sequence with predefined storage
		/// </summary>
		[Pure]
		internal Sequence(T[] array)
		{
			Contract.Requires(!ReferenceEquals(array, null));

			this.array=new T[array.Length];
			this.array = array;
		}


//------------------------------------------------------------------------------
// Properties
//------------------------------------------------------------------------------

		/// <summary>
		/// Is <paramref name="x"/> contained?
		/// </summary>
		[Pure]
		public bool Has(T x)
		{
			return array.Contains(x);
		}

		/// <summary>
		/// Is the sequence empty?
		/// </summary>
		[Pure]
		public bool IsEmpty()
		{
			return array.Length == 0;
		}

		/// <summary>
		/// Are all values equal to <paramref name="c"/>?
		/// </summary>
		[Pure]
		public bool IsConstant(T c)
		{
			return Array.TrueForAll(array, x => Equals(x, c));
		}


//------------------------------------------------------------------------------
// Elements
//------------------------------------------------------------------------------

		/// <summary>
		/// Value at position <paramref name="i"/>
		/// </summary>
		[Pure]
		public T Item(int i)
		{
			Contract.Requires(Domain[i]);
			return array[i];
		}

		/// <summary>
		/// Indexer alias for Item
		/// </summary>
		[Pure]
		[System.Runtime.CompilerServices.IndexerName("Item_")]
		public T this[int i]
		{
			get 
			{
				return Item(i); 
			}
		}


//------------------------------------------------------------------------------
// Conversion
//------------------------------------------------------------------------------

		/// <summary>
		/// Set of indexes
		/// </summary>
		[Pure]
		public Interval Domain
		{
			get { return new Interval(0, Count - 1); }
		}

		/// <summary>
		/// Set of values
		/// </summary>
		/// <returns></returns>
		[Pure]
		public Set<T> Range
		{
			get { return new Set<T>(array.Distinct().ToArray()); }
		}

		// newly implemented
		/// <summary>
		/// Bag of sequence values
		/// </summary>
		/// <returns></returns>
		[Pure]
		public Bag<T> ToBag()
		{
			Bag<T> bag = new Bag<T>();

			for (int i = 0; i < array.Length; i++)
			{
				bag = bag.Extended(array[i]);
			}

			return bag;
		}


//------------------------------------------------------------------------------
// Measurement
//------------------------------------------------------------------------------

		/// <summary>
		/// Number of elements
		/// </summary>
		[Pure]
		public int Count
		{
			get { return array.Length; }
		}

		/// <summary>
		/// How many times does <paramref name="x"/> occur
		/// </summary>
		[Pure]
		public int Occurrences(T x)
		{
			return array.Count(y => Equals(x, y));
		}


//------------------------------------------------------------------------------
// Comparison
//------------------------------------------------------------------------------

		/// <summary>
		/// Is <paramref name="obj"/> a set with the same elements in the same order?
		/// </summary>
		[Pure]
		public override bool Equals(object obj)
		{
			Sequence<T> seq = obj as Sequence<T>;
			return !ReferenceEquals(seq, null) && array.SequenceEqual(seq.array);
		}

		// Not part of API => no xml comment
		[Pure]
		public override int GetHashCode()
		{
			return array.GetHashCode();
		}

		/// <summary>
		/// Are the two objects equal? (Operator is equivalent to the |=| alias in Eiffel)
		/// </summary>
		[Pure]
		public static bool operator ==(Sequence<T> s1, Sequence<T> s2)
		{
			return (ReferenceEquals(s1, null) & ReferenceEquals(s2, null)) |
				(!ReferenceEquals(s1, null) && s1.Equals(s2));
		}

		/// <summary>
		/// Are the two objects not equal?
		/// </summary>
		[Pure]
		public static bool operator !=(Sequence<T> s1, Sequence<T> s2)
		{
			return !(s1 == s2);
		}

		/// <summary>
		/// Is this sequence prefix of <paramref name="other"/>?
		/// </summary>
		[Pure]
		public bool IsPrefixOf(Sequence<T> other)
		{
			Contract.Requires(other != null);
			return Count <= other.Count && array.SequenceEqual(other.array.Take(Count));
		}


//------------------------------------------------------------------------------
// Decomposition
//------------------------------------------------------------------------------

		/// <summary>
		/// First element
		/// </summary>
		[Pure]
		public T First()
		{
			Contract.Requires(!IsEmpty());
			return Item(0);
		}

		/// <summary>
		/// Last element
		/// </summary>
		[Pure]
		public T Last()
		{
			Contract.Requires(!IsEmpty());
			return Item(Count - 1);
		}

		/// <summary>
		/// This sequence without the first element
		/// </summary>
		[Pure]
		public Sequence<T> ButFirst()
		{
			Contract.Requires(!IsEmpty());
			return Interval(1, Count - 1);
		}

		/// <summary>
		/// This sequence without the last element
		/// </summary>
		[Pure]
		public Sequence<T> ButLast()
		{
			Contract.Requires(!IsEmpty());
			return Interval(0, Count - 2);
		}

		/// <summary>
		/// Prefix up to <paramref name="upper"/>
		/// </summary>
		[Pure]
		public Sequence<T> Front(int upper)
		{
			return Interval(0, upper);
		}

		/// <summary>
		/// Suffix from <paramref name="lower"/>
		/// </summary>
		[Pure]
		public Sequence<T> Tail(int lower)
		{
			return Interval(lower, Count - 1);
		}

		/// <summary>
		/// Subsequence from <paramref name="lower"/> to <paramref name="upper"/>.
		/// </summary>
		[Pure]
		public Sequence<T> Interval(int lower, int upper)
		{
			lower = System.Math.Max(lower, 0);
			upper = System.Math.Min(upper, Count - 1);
			upper = System.Math.Max(lower, upper);
			int count = upper - lower + 1;

			return new Sequence<T>(array.Skip(lower).Take(count).ToArray());
		}

		/// <summary> 
		/// This sequence with element at position <paramref name="i"/> removed
		/// </summary>
		[Pure]
		public Sequence<T> RemovedAt(int i)
		{
			Contract.Requires(Domain[i]);

			T[] a = new T[array.Length - 1];
			Array.Copy(array, 0, a, 0, i);
			Array.Copy(array, i + 1, a, i, array.Length - i - 1);

			return new Sequence<T>(a);
		}

		// newly added
		/// <summary>
		/// Current sequence with all elements with indexes outside of <paramref name="subdomain"/> removed.
		/// </summary>
		[Pure]
		public Sequence<T> Restricted(Set<int> subdomain)
		{
			Contract.Requires(!ReferenceEquals(subdomain, null));

			T[] tmp = new T[array.Length];
			int j = 0;

			for (int i = 0; i < array.Length; i++)
			{
				if (subdomain[i])
				{
					tmp[j] = array[i];
					j++;
				}
			}
			Array.Resize(ref tmp, j);
			return new Sequence<T>(tmp);
		}

		// newly added
		/// <summary>
		/// Current sequence with all elements with indexes from <paramref name="subdomain"/> removed.
		/// </summary>
		[Pure]
		public Sequence<T> Removed(Set<int> subdomain)
		{
			Contract.Requires(!ReferenceEquals(subdomain, null));
			return Restricted(Domain - subdomain);
		}


//------------------------------------------------------------------------------
// Modification
//------------------------------------------------------------------------------

		// no operator
		/// <summary>
		/// Current sequence extended with <paramref name="x"/> at the end
		/// </summary>
		[Pure]
		public Sequence<T> Extended(T x)
		{ 
			/*
			T[] a = new T[array.Length + 1];
			Array.Copy(array, a, array.Length);
			a[a.Length - 1] = x;
			return new Sequence<T>(a);
				*/
			return ExtendedAt(array.Length, x);
		}

		/// <summary>
		/// Operator equivalent for extended operation <seealso cref="Extended(T)"></seealso>>
		/// </summary>
		[Pure]
		public static Sequence<T> operator &(Sequence<T> s1, T t)
		{
			Contract.Requires(!ReferenceEquals(s1, null));
			Contract.Requires(!ReferenceEquals(t, null));
			return s1.Extended(t);
		}

		// newly added
		/// <summary>
		/// Current sequence with <paramref name="x"/> inserted at position <param name="i"/>
		/// </summary>
		/// 
		[Pure]
		public Sequence<T> ExtendedAt(int i, T x)
		{
			Contract.Requires(i >= 0);
			Contract.Requires(i <= Count);

			T[] a = new T[array.Length + 1];
			// elements before x
			Array.Copy(array, a, i);
			// x somewhere in the middle
			a[i] = x;
			// finally the rest
			Array.Copy(array, i, a, i+1, a.Length - i - 1);

			return new Sequence<T>(a);
		}

		/// <summary>
		/// Current sequence prepended with <paramref name="x"/> at the beginning
		/// </summary>
		[Pure]
		public Sequence<T> Prepended(T x)
		{
			T[] a = new T[array.Length + 1];
			Array.Copy(array, 0, a, 1, array.Length);
			a[0] = x;
			return new Sequence<T>(a);
		}

		/// <summary>
		/// The concatenation of the this sequence and <paramref name="other"/>
		/// </summary>
		[Pure]
		public Sequence<T> Concatenation (Sequence<T> other)
		{
			Contract.Requires(!ReferenceEquals(other, null));
			return new Sequence<T>(array.Concat(other.array).ToArray());
		}

		/// <summary>
		/// Operator equivalent for concatenation operation <seealso cref="Concatenation (Sequence<T>)"></seealso>>
		/// </summary>
		[Pure]
		public static Sequence<T> operator +(Sequence<T> s1, Sequence<T> s2)
		{
			Contract.Requires(!ReferenceEquals(s1, null));
			Contract.Requires(!ReferenceEquals(s2, null));
			return s1.Concatenation(s2);
		}

		/// <summary>
		/// This sequence with <paramref name="x"/> at position <paramref name="i"/>
		/// </summary>
		[Pure]
		public Sequence<T> ReplacedAt(int i, T x)
		{
			Contract.Requires (Domain[i]);

			T[] a = new T[array.Length];
			Array.Copy(array, a, array.Length);
			a[i] = x;

			return new Sequence<T>(a);
		}

		/// <summary>
		/// Relation of values in this sequence to their indexes
		/// </summary>
		[Pure]
		public Relation<T, int> Inverse()
		{ 
			return new Relation<T, int>(array, Domain.array);
		}

		// not part of eiffel api
		/// <summary>
		/// Reverses the order of the elements in a Sequence.
		/// </summary>
		[Pure]
		public Sequence<T> Reverse()
		{
			return new Sequence<T>(array.Reverse().ToArray());
		}


//------------------------------------------------------------------------------
// Implementation
//------------------------------------------------------------------------------

		// Element storage
		internal T[] array;


//------------------------------------------------------------------------------
// Invariant
//------------------------------------------------------------------------------

		[ContractInvariantMethod]
		private void ObjectInvariant()
		{
			Contract.Invariant(array != null);
		}
	}
}
