﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics.Contracts;

namespace MML
{
	/// <summary>
	/// Finite set
	/// </summary>
	/// <typeparam name="T">Type of set elements</typeparam>
	public class Set<T> : System.Collections.IEnumerable
	{

		// Not part of API => no xml comment
		[Pure]
		public System.Collections.IEnumerator GetEnumerator()
		{
			return array.GetEnumerator();
		}


//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
	
		// default create
		/// <summary>
		/// Create an empty set
		/// </summary>
		[Pure]
		public Set()
		{
			array = new T[0];
		}

		// singleton
		/// <summary>
		/// Create a singleton set
		/// </summary>
		/// <param name="x">The only element of the set</param>
		[Pure]
		public Set(T x)
		{
			array = new T[1];
			array[0] = x;
		}

		// make from array
		/// <summary>
		/// Create a set with predefined storage
		/// </summary>
		[Pure]
		internal Set(T[] array)
		{
			Contract.Requires(array != null);
			// no_duplicates
			Contract.Requires(array.Distinct().Count() == array.Length);
			this.array = array;
		}


//------------------------------------------------------------------------------
// Properties
//------------------------------------------------------------------------------

		/// <summary>
		/// Is <paramref name="x"/> contained?
		/// </summary>
		[Pure]
		public bool Has(T x)
		{
			return array.Contains(x);
		}

		/// <summary>
		/// Indexer alias for Has
		/// </summary>
		[Pure]
		public bool this[T x]
		{
			get { return Has(x); }
		}

		/// <summary>
		/// Is the set empty?
		/// </summary>
		[Pure]
		public bool IsEmpty()
		{
			return array.Length == 0;
		}

		/// <summary>
		/// Does <paramref name="test"/> hold for all elements?
		/// </summary>
		[Pure]
		public bool ForAll(Predicate<T> test)
		{
			Contract.Requires(test != null);
			return Array.TrueForAll(array, test);
		}

		/// <summary>
		/// Does <paramref name="test"/> hold for at least one element?
		/// </summary>
		[Pure]
		public bool Exists(Predicate<T> test)
		{
			Contract.Requires(test != null);
			return Array.Exists(array, test);
		}


//------------------------------------------------------------------------------
// Elements
//------------------------------------------------------------------------------

		/// <summary>
		/// Arbitrary element
		/// </summary>
		[Pure]
		public T AnyItem()
		{
			Contract.Requires(!IsEmpty());
			return array[0];
		}

		public delegate bool OrderType(T x, T y);

		/// <summary>
		/// Least element with respect to <paramref name="order"/>.
		/// </summary>
		[Pure]
		public T Extremum(OrderType order)
		{
			Contract.Requires(order != null);
			Contract.Requires(!IsEmpty());

			T res = array[0];

			for (int i = 1; i < array.Length; i++)
			{
				if (order(array[i], res))
					res = array[i];
			}

			return res;
		}


//------------------------------------------------------------------------------
// Subsets
//------------------------------------------------------------------------------

		/// <summary>
		/// Set of all elements that satisfy <paramref name="test"/>
		/// </summary>
		[Pure]
		public Set<T> Filtered(Predicate<T> test)
		{
			Contract.Requires(test != null);
			return new Set<T>(Array.FindAll(array, test));
		}

		/// <summary>
		/// Operator equivalent for filtered operation <seealso cref="Filtered(Predicate<T>)"></seealso>>
		/// </summary>
		[Pure]
		public static Set<T> operator |(Set<T> s1, Predicate<T> p)
		{
			Contract.Requires(!ReferenceEquals(s1, null));
			Contract.Requires(!ReferenceEquals(p, null));
			return s1.Filtered(p);
		}


//------------------------------------------------------------------------------
// Measurement
//------------------------------------------------------------------------------

		/// <summary>
		/// Cardinality
		/// </summary>
		[Pure]
		public int Count
		{
			get { return array.Length; }
		}


//------------------------------------------------------------------------------
// Comparison
//------------------------------------------------------------------------------

		/// <summary>
		/// Is <paramref name="obj"/> a set with the same elements?
		/// </summary>
		[Pure]
		public override bool Equals(object obj)
		{
			Set<T> set = obj as Set<T>;
			return !ReferenceEquals(set, null) && Count == set.Count && IsSubsetOf(set);
		}

		// Not part of API => no xml comment
		[Pure]
		public override int GetHashCode()
		{
			return array.GetHashCode();
		}

		/// <summary>
		/// Are the two objects equal? (Operator is equivalent to the |=| alias in Eiffel)
		/// </summary>
		[Pure]
		public static bool operator ==(Set<T> s1, Set<T> s2)
		{
			return (ReferenceEquals(s1, null) && ReferenceEquals(s2, null)) |
				(!ReferenceEquals(s1, null) && s1.Equals(s2));
		}

		/// <summary>
		/// Are the two objects not equal?
		/// </summary>
		[Pure]
		public static bool operator !=(Set<T> s1, Set<T> s2)
		{
			return !(s1 == s2);
		}

		/// <summary>
		///  Does <paramref name="other"/> have all elements of this set?
		/// </summary>
		[Pure]
		public bool IsSubsetOf(Set<T> other)
		{
			Contract.Requires(!ReferenceEquals(other, null));
			return ForAll(other.Has);
		}

		/// <summary>
		/// Operator equivalent for IsSubsetOf operation <seealso cref="IsSubsetOf(Set<T>)"></seealso>>
		/// </summary>
		[Pure]
		public static bool operator <=(Set<T> s1, Set<T> s2)
		{
			Contract.Requires(!ReferenceEquals(s1, null));
			Contract.Requires(!ReferenceEquals(s2, null));
			return s1.IsSubsetOf(s2);
		}

		/// <summary>
		/// Does this set have all elements of <paramref name="other"/>?
		/// </summary>
		[Pure]
		public bool IsSupersetOf(Set<T> other)
		{
			Contract.Requires(!ReferenceEquals(other, null));
			return other.IsSubsetOf(this);
		}

		/// <summary>
		/// Operator equivalent for IsSupersetOf operation <seealso cref="IsSupersetOf(Set<T>)"></seealso>>
		/// </summary>
		[Pure]
		public static bool operator >=(Set<T> s1, Set<T> s2)
		{
			Contract.Requires(!ReferenceEquals(s1, null));
			Contract.Requires(!ReferenceEquals(s2, null));
			return s1.IsSupersetOf(s2);
		}

		/// <summary>
		/// Do no elements of <paramref name="other"/> occur in this set?
		/// </summary>
		[Pure]
		public bool Disjoint(Set<T> other)
		{
			Contract.Requires(!ReferenceEquals(other, null));
			return !other.Exists(this.Has);
		}


//------------------------------------------------------------------------------
// Modification
//------------------------------------------------------------------------------

		/// <summary>
		/// This set extended with <paramref name="x"/> if absent
		/// </summary>
		[Pure]
		public Set<T> Extended(T x)
		{
			if (!Has(x))
			{
				T[] a = new T[array.Length + 1];
				Array.Copy(array, a, array.Length);
				a[a.Length - 1] = x;

				return new Set<T>(a);
			}
			else
			{
				return this;
			}
		}

		/// <summary>
		/// Operator equivalent for extended operation <seealso cref="Extended(T)"></seealso>>
		/// </summary>
		[Pure]
		public static Set<T> operator &(Set<T> s1, T t)
		{
			Contract.Requires(!ReferenceEquals(s1, null));
			Contract.Requires(!ReferenceEquals(t, null));
			return s1.Extended(t);
		}

		/// <summary>
		/// Current set with <paramref name="x"/> removed if present
		/// </summary>
		[Pure]
		public Set<T> Removed(T x)
		{
			return new Set<T>(Array.FindAll(array, y => !Equals(x, y)));
		}


		/// <summary>
		/// Operator equivalent for removed operation <seealso cref="Removed(T)"></seealso>>
		/// </summary>
		[Pure]
		public static Set<T> operator /(Set<T> s1, T t)
		{
			Contract.Requires(!ReferenceEquals(s1, null));
			Contract.Requires(!ReferenceEquals(t, null));
			return s1.Removed(t);
		}

		/// <summary>
		/// Set of values contained in either this set or <paramref name="other"/>
		/// </summary>
		[Pure]
		public Set<T> Union(Set<T> other)
		{
			Contract.Requires(!ReferenceEquals(other, null));

			Set<T> res = Difference(other);
			Array.Resize(ref res.array, res.array.Length + other.array.Length);
			Array.Copy(other.array, 0, res.array, res.Count - other.Count, other.Count);

			return res;
		}

		/// <summary>
		/// Operator equivalent for union operation <seealso cref="Union(Set<T>)"></seealso>>
		/// </summary>
		[Pure]
		public static Set<T> operator +(Set<T> s1, Set<T> s2)
		{
			Contract.Requires(!ReferenceEquals(s1, null));
			Contract.Requires(!ReferenceEquals(s2, null));
			return s1.Union(s2);
		}

		/// <summary>
		/// Set of values contained in both this set and <paramref name="other"/>
		/// </summary>
		[Pure]
		public Set<T> Intersection(Set<T> other)
		{
			Contract.Requires(!ReferenceEquals(other, null));
			return new Set<T>(Array.FindAll(array, other.Has));
		}

		/// <summary>
		/// Operator equivalent for intersection operation <seealso cref="Intersection(Set<T>)"></seealso>>
		/// </summary>
		[Pure]
		public static Set<T> operator *(Set<T> s1, Set<T> s2)
		{
			Contract.Requires(!ReferenceEquals(s1, null));
			Contract.Requires(!ReferenceEquals(s2, null));
			return s1.Intersection(s2);
		}

		/// <summary>
		/// Set of values contained in this set but not in <paramref name="other"/>
		/// </summary>
		[Pure]
		public Set<T> Difference(Set<T> other)
		{
			Contract.Requires(!ReferenceEquals(other, null));
			return new Set<T>(Array.FindAll(array, x => !other.Has(x)));
		}

		/// <summary>
		/// Operator equivalent for difference operation <seealso cref="Difference(Set<T>)"></seealso>>
		/// </summary>
		[Pure]
		public static Set<T> operator -(Set<T> s1, Set<T> s2)
		{
			Contract.Requires(!ReferenceEquals(s1, null));
			Contract.Requires(!ReferenceEquals(s2, null));
			return s1.Difference(s2);
		}

		/// <summary>
		/// Set of values contained in either this set or <paramref name="other"/>, but not in both
		/// </summary>
		[Pure]
		public Set<T> SymDifference(Set<T> other)
		{
			Contract.Requires(!ReferenceEquals(other, null));
			return Union(other).Difference(Intersection(other));
		}

		/// <summary>
		/// Operator equivalent for symmetric difference operation <seealso cref="SymDifference(Set<T>)"></seealso>>
		/// </summary>
		[Pure]
		public static Set<T> operator ^(Set<T> s1, Set<T> s2)
		{
			Contract.Requires(!ReferenceEquals(s1, null));
			Contract.Requires(!ReferenceEquals(s2, null));
			return s1.SymDifference(s2);
		}


//------------------------------------------------------------------------------
// Implementation
//------------------------------------------------------------------------------

		/// Element storage
		internal T[] array;


//------------------------------------------------------------------------------
// Invariant
//------------------------------------------------------------------------------

		[ContractInvariantMethod]
		private void ObjectInvariant() 
		{ 
			Contract.Invariant(array != null);
			Contract.Invariant(array.Distinct().Count() == array.Length);
		}
	}
}
