﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics.Contracts;

namespace MML
{
	/// <summary>
	/// Finite map
	/// </summary>
	/// <typeparam name="K">Type of key elements</typeparam>
	/// <typeparam name="V">Type of value elements</typeparam>
	public class Map<K, V>
	{

//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------

		// default_create
		/// <summary>
		/// Create an empty map
		/// </summary>
		[Pure]
		public Map()
		{
			keys = new K[0];
			values = new V[0];
		}

		// singleton
		/// <summary>
		/// Create a singleton map
		/// </summary>
		/// <param name="key">The only key in the map</param>
		/// <param name="value">The only value in the map</param>
		[Pure]
		public Map(K key, V value)
		{
			keys = new K[1];
			values = new V[1];
			keys[0] = key;
			values[0] = value;
		}

		// make_from_arrays
		/// <summary>
		/// Create a map with predefined storage
		/// </summary>
		/// <param name="keys">The keys in the map</param>
		/// <param name="values">The values in the map</param>
		[Pure]
		internal Map(K[] keys, V[] values)
		{
			// ks_exists, vs_exists
			Contract.Requires(keys != null);
			Contract.Requires(values != null);
			Contract.Requires(keys.Length == values.Length);
			// ks_has_no_duplicates
			Contract.Requires(keys.Distinct().Count() == keys.Length);

			this.keys = keys;
			this.values = values;
		}


//------------------------------------------------------------------------------
// Properties
//------------------------------------------------------------------------------

		/// <summary>
		/// Does map contain value <paramref name="x"/>
		/// </summary>
		[Pure]
		public bool Has(V x)
		{
			return values.Contains(x);
		}

		// not in Eiffel API
		/// <summary>
		/// Does map contain key <paramref name="x"/>
		/// </summary>
		[Pure]
		public bool Has(K x)
		{
			return keys.Contains(x);
		}

		/// <summary>
		/// Is map empty?
		/// </summary>
		[Pure]
		public bool IsEmpty()
		{
			return keys.Length == 0;
		}

		/// <summary>
		/// Are all values equal to <paramref name="c"/>?
		/// </summary>
		[Pure]
		public bool IsConstant(V c)
		{
			return Array.TrueForAll(values, x => Equals(x, c));
		}


//------------------------------------------------------------------------------
// Elements
//------------------------------------------------------------------------------

		/// <summary>
		/// Value associated with <paramref name="k"/>
		/// </summary>
		[Pure]
		public V Item(K k)
		{
			Contract.Requires(Domain[k]);
			return values[Array.IndexOf(keys, k)];
		}

		/// <summary>
		/// Indexer alias for Item
		/// </summary>
		[System.Runtime.CompilerServices.IndexerName("Item_")]
		[Pure]
		public V this[K k]
		{
			get 
			{
				return Item(k); 
			}
		}


//------------------------------------------------------------------------------
// Conversion
//------------------------------------------------------------------------------

		/// <summary>
		/// Set of keys
		/// </summary>
		[Pure]
		public Set<K> Domain
		{
			get { return new Set<K>(keys); }
		}

		/// <summary>
		/// Set of values
		/// </summary>
		[Pure]
		public Set<V> Range
		{
			get { return new Set<V>(values.Distinct().ToArray()); }
		}

		/// <summary>
		/// Set of values corresponding to keys in <paramref name="subdomain"/>
		/// </summary>
		[Pure]
		public Set<V> Image(Set<K> subdomain)
		{
			Contract.Requires(!ReferenceEquals(subdomain, null));
			return Restricted(subdomain).Range;
		}

		/// <summary>
		/// Sequence of images of <paramref name="s"/> elements under this map
		/// </summary>
		[Pure]
		public Sequence<V> SequenceImage(Sequence<K> s)
		{
			Contract.Requires(!ReferenceEquals(s, null));
			return new Sequence<V>(Array.ConvertAll<K, V>(
									Array.FindAll(s.array, Domain.Has), Item));
		}

		// Newly implemented
		/// <summary>
		/// Bag of map values
		/// </summary>
		[Pure]
		public Bag<V> ToBag()
		{
			Bag<V> bag = new Bag<V>();

			for (int i = 0; i < values.Length; i++)
			{
				bag = bag.Extended(values[i]);
			}

			return bag;
		}


//------------------------------------------------------------------------------
// Measurement
//------------------------------------------------------------------------------

		/// <summary>
		///  Map cardinality
		/// </summary>
		[Pure]
		public int Count
		{
			get { return keys.Length; }
		}


//------------------------------------------------------------------------------
// Comparison
//------------------------------------------------------------------------------

		/// <summary>
		/// Is <paramref name="obj"/> a map with the same key-value pairs?
		/// </summary>
		[Pure]
		public override bool Equals(object obj)
		{
			Map<K, V> map = obj as Map<K, V>;
			return !ReferenceEquals(map, null) && Count == map.Count &&
										Enumerable.Range(0, keys.Length).All(
										i => Equals (map[keys[i]], values[i]));
		}

		// Not part of API => no xml comment
		[Pure]
		public override int GetHashCode()
		{
			return keys.GetHashCode() ^ values.GetHashCode();
		}

		/// <summary>
		/// Are the two objects equal? (Operator is equivalent to the |=| alias in Eiffel)
		/// </summary>
		[Pure]
		public static bool operator ==(Map<K, V> m1, Map<K, V> m2)
		{
			return (ReferenceEquals(m1, null) & ReferenceEquals(m2, null)) |
				(!ReferenceEquals(m1, null) && m1.Equals(m2));
		}

		/// <summary>
		/// Are the two objects not equal?
		/// </summary>
		[Pure]
		public static bool operator !=(Map<K, V> m1, Map<K, V> m2)
		{
			return !(m1 == m2);
		}


//------------------------------------------------------------------------------
// Modification
//------------------------------------------------------------------------------

		/// <summary>
		/// Current map with <paramref name="v"/> associated with <paramref name="k"/>.
		/// If <paramref name="k"/> already exists, the value is replaced, otherwise added.
		/// </summary>
		[Pure]
		public Map<K, V> Updated(K k, V v)
		{
			int i = Array.IndexOf(keys, k);
			if (i >= 0)
			{
				// found, just replace value
				V[] vs = new V[values.Length];
				Array.Copy(values, vs, values.Length);
				vs[i] = v;
				return new Map<K, V>(keys, vs);
			}
			else
			{
				// not found, add new pair
				K[] ks = new K[keys.Length + 1];
				Array.Copy(keys, ks, keys.Length);
				ks[ks.Length - 1] = k;

				V[] vs = new V[values.Length + 1];
				Array.Copy(values, vs, values.Length);
				vs[vs.Length - 1] = v;
				return new Map<K, V>(ks, vs);
			}
		}

		/// <summary>
		/// Current map without key <paramref name="k"/> and the corresponding value
		/// </summary>
		[Pure]
		public Map<K, V> Removed(K k)
		{
			int i = Array.IndexOf(keys, k);
			if (i >= 0)
			{
				// found: remove key,value pair
				K[] ks = new K[keys.Length - 1];
				Array.Copy(keys, 0, ks, 0, i);
				Array.Copy(keys, i + 1, ks, i, keys.Length - i - 1);

				V[] vs = new V[values.Length - 1];
				Array.Copy(values, 0, vs, 0, i);
				Array.Copy(values, i, vs, i, values.Length - i - 1);
				return new Map<K, V>(ks, vs);
			}
			else
			{
				return this;
			}
		}

		/// <summary>
		/// Map that consists of all key-value pairs of this map whose key is in <paramref name="subdomain"/>
		/// </summary>
		[Pure]
		public Map<K, V> Restricted(Set<K> subdomain)
		{
			Contract.Requires(!ReferenceEquals(subdomain, null));

			K[] ks = new K[keys.Length];
			V[] vs = new V[values.Length];
			int j = 0;

			for (int i = 0; i < keys.Length; i++)
			{
				if (subdomain[keys[i]])
				{
					ks[j] = keys[i];
					vs[j] = values[i];
					j++;
				}
			}

			Array.Resize(ref ks, j);
			Array.Resize(ref vs, j);
			return new Map<K, V>(ks, vs);
		}

		/// <summary>
		/// Operator equivalent for restricted operation <seealso cref="Restricted(Set<K>)"></seealso>>
		/// </summary>
		[Pure]
		public static Map<K, V> operator |(Map<K, V> m, Set<K> s)
		{
			Contract.Requires(!ReferenceEquals(m, null));
			Contract.Requires(!ReferenceEquals(s, null));
			return m.Restricted(s);
		}

		/// <summary>
		/// Map that is equal to <paramref name="other"/> on its domain 
		/// and to this map on its domain minus the domain of <paramref name="other"/>
		/// </summary>
		[Pure]
		public Map<K, V> Override(Map<K, V> other)
		{
			Contract.Requires(!ReferenceEquals(other, null));

			// Take all key/value pairs from other first
			K[] ks = new K[keys.Length + other.keys.Length];
			V[] vs = new V[values.Length + other.values.Length];
			Array.Copy(other.keys, ks, other.keys.Length);
			Array.Copy(other.values, vs, other.values.Length);

			// Add all pairs from this where the key is not in other
			int j = other.keys.Length;

			for (int i = 0; i < keys.Length; i++)
			{
				if (!other.Domain[keys[i]])
				{
					ks[j] = keys[i];
					vs[j] = values[i];
					j++;
				}
			}

			Array.Resize(ref ks, j);
			Array.Resize(ref vs, j);
			return new Map<K, V>(ks, vs);
		}

		/// <summary>
		/// Operator equivalent for override operation <seealso cref="Override(Map<K, V>)"></seealso>>
		/// </summary>
		[Pure]
		public static Map<K, V> operator +(Map<K, V> m1, Map<K, V> m2)
		{
			Contract.Requires(!ReferenceEquals(m1, null));
			Contract.Requires(!ReferenceEquals(m2, null));
			return m1.Override(m2);
		}

		/// <summary>
		/// Relation consisting of inverted pairs from this map
		/// </summary>
		[Pure]
		public Relation<V, K> Inverse()
		{
			return new Relation<V, K>(values, keys);
		}


//------------------------------------------------------------------------------
// Implementation
//------------------------------------------------------------------------------

		// Storage for keys
		internal K[] keys;

		// Storage for values
		internal V[] values;


//------------------------------------------------------------------------------
// Invariant
//------------------------------------------------------------------------------

		[ContractInvariantMethod]
		private void ObjectInvariant()
		{
			Contract.Invariant(keys != null);
			Contract.Invariant(values != null);
			Contract.Invariant(keys.Length == values.Length);
			Contract.Invariant(keys.Distinct().Count() == keys.Length);
		}
	}
}
