﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace MML
{
	/// <summary>
	/// Closed integer intervals
	/// </summary>
	public class Interval : Set<int>
	{
//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------

		// from_range create
		/// <summary>
		/// Create interval [<paramref name="l"/>, <paramref name="u"/>]
		/// </summary>
		[Pure]
		public Interval(int l, int u)
		{
			if (l <= u)
			{
				array = new int[u - l + 1];
				for (int i = 0; i < array.Length; i++)
				{
					array[i] = l + i;
				}
			}
			else
			{
				array = new int[0];
			}
		}

	// from_tuple
		/// <summary>
		/// Create interval from tuple[<paramref name="tuple"/>]
		/// </summary>
		[Pure]
		public Interval(Tuple<int, int> tuple) : this(tuple.Item1, tuple.Item2)
		{

		}

//------------------------------------------------------------------------------
// Access
//------------------------------------------------------------------------------

		/// <summary>
		/// Lower bound
		/// </summary>
		[Pure]
		public int Lower()
		{
			Contract.Requires(!IsEmpty());
			return array[0];
		}

		/// <summary>
		/// Upper bound
		/// </summary>
		[Pure]
		public int Upper()
		{
			Contract.Requires(!IsEmpty());
			return array [array.Length - 1];
		}
	}
}
